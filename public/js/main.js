$(document).ready(function() {
	$(".next").on('click', function(event) {
		document.querySelector('#aboutMe').scrollIntoView({ 
			behavior: 'smooth'
		});
	});

	$(".lightgallery").lightGallery();

	$( ".menu-btn" ).click(function() {
		$( '.menu' ).toggleClass( "open" );
	});
});