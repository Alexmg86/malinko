<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="@yield('pageDesc')">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('pageTitle')</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="{{ mix('css/all.css')}}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <header class="{{Request::is('/') ? 'main' : 'other'}}">
            <div class="container">
                <a href="/" class="logo"></a>
                <a href="#" class="menu-btn"></a>
                <ul class="menu">
                    <li><a href="/#aboutMe">Коротко о главном</a></li>
                    <li><a href="/tvorchestvo" class="{{Request::is('tvorchestvo') ? 'active' : ''}}">Творчество</a></li>
                    <li><a href="/gallery" class="{{Request::is('gallery') ? 'active' : ''}}">Фото</a></li>
                    <li><a href="/contacts" class="{{Request::is('contacts') ? 'active' : ''}}">Контакты</a></li>
                </ul>
            </div>
        </header>

        <div class="main-box">
            @yield('content')
        </div>

        <footer>
            <div class="container">
                <hr>
                <p>© Владимир Малинко, @php {{echo date("Y");}} @endphp. Все права защищены.</p>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/all.js') }}"></script>
    @yield('footer')
</body>
</html>
