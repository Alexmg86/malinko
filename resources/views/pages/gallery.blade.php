@extends('layouts.app')
@section('pageTitle', 'Владимир Малинко - Фото')
@section('content')
<section class="easy">
    <div class="container">
        <h1>ФОТО</h1>
    </div>

    <div class="container">
        <div class="row pad-easy-30 alboum-list">
            <div class="col-sm-12">
                <div class="lightgallery row">
                    <a href="img/skifs/skifs1.jpg" class="col-sm-4 col-xs-6">
                        <img class="img-rounded" src="img/skifs/skifs1_small.jpg" />
                    </a>
                    <a href="img/mn/mn3.jpg" class="col-sm-4 col-xs-6">
                        <img class="img-rounded" src="img/mn/mn3_small.jpg" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
