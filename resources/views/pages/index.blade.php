@extends('layouts.app')
@section('pageTitle', 'Владимир Малинко - Главная')
@section('content')
<section class="first">
    <a class="next" href="javascript:void(0);">Далее</a>
</section>

<section class="easy" id="aboutMe">
    <div class="container">
        <h1>Коротко о главном</h1>
        <div class="pad-easy-30">
            <p>Автор и исполнитель своих песен <strong>Владимир Малинко</strong> родился в 1961 году в Алма-Ате. Окончив музыкальную школу по классу «фортепиано», Владимир в 15 лет увлёкся рок-музыкой, начал сочинять песни на стихи русских и зарубежных поэтов. Уже через год появились первые серьёзные музыкальные произведения на стихи Александра Блока, Уильяма Шекспира и Джона Байрона.<p>
            <p>Стиль музыки Владимира Малинко – мелодичный «тяжёлый рок». В период учёбы в Алма-Атинском педагогическом институте иностранных языков с 1978 по 1983 годы Владимир написал более 50 песен на стихи Франсуа Вийона, Поля Верлена, Жана Оврэ, а также на свои собственные стихи. В студенческие годы Владимир Малинко выступал в самодеятельных музыкальных ансамблях, исполняя свои произведения на различных творческих конкурсах. Песни на стихи Франсуа Вийона Владимир неоднократно исполнял в институте, на городских конкурсах художественной самодеятельности в 1982-1983,1985 годах.<p>
            <p>В 1986-1988 годах Владимир сочинил несколько композиций на стихи Марины Цветаевой, Семёна Кирсанова, литовского поэта Эдуардаса Межелайтиса, эстонского поэта Арви Сийга в переводах русских поэтов Бориса Авсарагова, Александра Зорина, Риммы Казаковой.<p>
            <p>В настоящее время Владимир Малинко живёт в Ленинградской области, где записывает в собственной домашней студии авторские музыкальные альбомы.<p>
            <p>В 2013 году увидел свет CD-альбом <strong>«СКИФЫ»</strong> на стихи Александра Блока, куда вошли песни, написанные в основном в 1977-1979 годах. В 2014 году был записан альбом <strong>«ТЕНЬ АНГЕЛА»</strong>, в котором Владимир исполнил песни на свои стихи. В 2015 году была завершена 25-летняя работа над рок-сюитой <strong>«ЭМБЛЕМЫ: ПРИМЕНИТЕЛЬНО К ЧЕЛОВЕКУ»</strong> на стихи литовского поэта Эдуардаса Межелайтиса в переводах русского поэта Юрия Кобрина. В декабре 2016 года Владимир Малинко записал на CD рок-сюиту <strong>«МУДРОСТЬ НЕБЕС»</strong> на стихи эстонского поэта Арви Сийга. В течение последующих 8 лет Владимир записал ещё 4 альбома на стихи русских поэтов Серебряного века и на свои собственные стихи. В 2024 году завершена многолетняя работа над рок-сюитой на стихи Франсуа Вийона.<p>
            <p>В творческом арсенале Владимира Малинко более 100 песен. Работа над записью новых музыкальных альбомов продолжается.<p>
            <p><strong>Композитор Владимир Малинко приглашает  к сотрудничеству певцов, музыкантов и организаторов концертов. Предложения можно отправить через страницу <a href="/contacts" class="link">«контакты»</a> и на электронный адрес: <a href="mailto:wladeymir@yandex.ru" class="link">wladeymir@yandex.ru</a></strong><p>
            <p><strong> Контактный телефон: 8(921) 897 34 53.</strong></p>
        </div>
    </div>
</section>
@stop
