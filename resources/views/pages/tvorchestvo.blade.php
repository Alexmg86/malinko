@extends('layouts.app')
@section('pageTitle', 'Владимир Малинко - творчество')
@section('content')
<section class="easy">
    <div class="container">
        <h1>Творчество</h1>
        <div class="pad-easy-30">
            <p>Автор искренне благодарит всех, кто проявил интерес к его произведениям, и с удовольствием дарит Вам своё творчество.<br>Вы можете послушать  альбом «СКИФЫ»  в МР3-формате.</p>
        </div>
    </div>

    <div class="play-alboum">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="lightgallery">
                        <a href="img/skifs/skifs5.jpg">
                            <img class="img-rounded" src="img/skifs/skifs5.jpg" />
                        </a>
                    </div>
                    <p class="desc-albom">Песен: 8, 39 мин.</p>
                </div>
                <div class="col-sm-8">
                    <p class="name">СКИФЫ</p>
                    <p class="desc">музыкальная рок-композиция Владимира Малинко на стихи поэта Александра Блока. Владимир Малинко – синтезаторы, вокал, аранжировки, запись и оформление альбома. В записи альбома принимала участие Дарина Рыжикова – вокал (2,4), бэк-вокал (6).</p>
                    <ul class="graphic music-list">
                        <li>
                            <a href="{{ asset('sound/track-1.mp3') }}">
                                <span class="count first">1</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Скифы</span>
                                <span class="duration">7:56</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-2.mp3') }}">
                                <span class="count first">2</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Балаганчик</span>
                                <span class="duration">2:17</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-3.mp3') }}">
                                <span class="count first">3</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Что же ты потупилась в смущеньи</span>
                                <span class="duration">6:00</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-4.mp3') }}">
                                <span class="count first">4</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Девушка пела в церковном хоре</span>
                                <span class="duration">4:56</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-5.mp3') }}">
                                <span class="count first">5</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Когда замрут отчаяние и злоба</span>
                                <span class="duration">4:22</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-6.mp3') }}">
                                <span class="count first">6</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">На поле Куликовом</span>
                                <span class="duration">3:48</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-7.mp3') }}">
                                <span class="count first">7</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Осенняя любовь  часть1</span>
                                <span class="duration">4:37</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('sound/track-8.mp3') }}">
                                <span class="count first">8</span>
                                <span class="play first glyphicon glyphicon-play"></span>
                                <span class="pause first glyphicon glyphicon-pause"></span>
                                <span class="name">Осенняя любовь части 2,3</span>
                                <span class="duration">6:00</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 30px;">
        <div class="row alboum-list">
            <div class="col-sm-4">
                <p class="name-small">Скифы</p>
                <p class="desc"><br><br><br></p>
                <div class="lightgallery row">
                    <a href="img/skifs/skifs2.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/skifs/skifs2_small.jpg" />
                    </a>
                    <a href="img/skifs/skifs3.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/skifs/skifs3_small.jpg" />
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="name-small">ТЕНЬ АНГЕЛА</p>
                <p class="desc">Владимир Малинко - вокал, синтезаторы, аранжировки, запись и оформление альбома - 2014 год<br><br></p>
                <div class="lightgallery row">
                    <a href="img/ta/ta1.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/ta/ta1_small.jpg" />
                    </a>
                    <a href="img/ta/ta2.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/ta/ta2_small.jpg" />
                    </a>
                    <a href="img/ta/ta3.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/ta/ta3_small.jpg" />
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="name-small">ЭМБЛЕМЫ: применительно к человеку</p>
                <p class="desc">Музыка Владимира Малинко<br>Стихи Эдуардаса Межелайтиса<br>Перевод с литовского Юрия Кобрина, год выпуска – 2015</p>
                <div class="lightgallery row">
                    <a href="img/embl/embl1.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/embl/embl1_small.jpg" />
                    </a>
                    <a href="img/embl/embl2.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/embl/embl2_small.jpg" />
                    </a>
                    <a href="img/embl/embl3.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/embl/embl3_small.jpg" />
                    </a>
                    <a href="img/embl/embl4.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/embl/embl4_small.jpg" />
                    </a>
                </div>
            </div>
        </div>
        <div class="row alboum-list">
            <div class="col-sm-4">
                <p class="name-small">МУДРОСТЬ НЕБЕС</p>
                <p class="desc">рок-сюита Владимира Малинко<br>на стихи Арви Сийга, год выпуска – 2016</p>
                <div class="lightgallery row">
                    <a href="img/mn/mn1.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/mn/mn1_small.jpg" />
                    </a>
                    <a href="img/mn/mn2.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/mn/mn2_small.jpg" />
                    </a>
                    <a href="img/mn/mn3.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/mn/mn3_small.jpg" />
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="name-small">Это было б неприлично,<br>если бы не было так весело</p>
                <p class="desc">на стихи поэта Саши Чёрного, год выпуска – 2019</p>
                <div class="lightgallery row" style="margin-top: 25px;">
                    <a href="img/2019/1.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/2019/1_small.jpg" />
                    </a>
                    <a href="img/2019/2.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2019/2_small.jpg" />
                    </a>
                    <a href="img/2019/3.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2019/3_small.jpg" />
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="name-small">Кукла наследника Тутти</p>
                <p class="desc">год выпуска – 2020<br><br></p>
                <div class="lightgallery row">
                    <a href="img/2020/4.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/2020/4_small.jpg" />
                    </a>
                    <a href="img/2020/5.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2020/5_small.jpg" />
                    </a>
                </div>
            </div>
        </div>

        <div class="row alboum-list">
            <div class="col-sm-4">
                <p class="name-small">Осенняя любовь</p>
                <p class="desc">год выпуска – 2023</p>
                <div class="lightgallery row">
                    <a href="img/2023/6.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/2023/6_small.jpg" />
                    </a>
                    <a href="img/2023/7.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2023/7_small.jpg" />
                    </a>
                    <a href="img/2023/8.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2023/8_small.jpg" />
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="name-small">У любви у нашей в детском возрасте</p>
                <p class="desc">год выпуска – 2024</p>
                <div class="lightgallery row">
                    <a href="img/2024/9.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/2024/9_small.jpg" />
                    </a>
                    <a href="img/2024/10.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2024/10_small.jpg" />
                    </a>
                    <a href="img/2024/11.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/2024/11_small.jpg" />
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="name-small">Завещание Франсуа Вийона</p>
                <p class="desc">двойной альбом, год выпуска – 2024</p>
                <div class="lightgallery row">
                    <a href="img/20242/12.jpg" class="col-sm-12 col-xs-6">
                        <img class="img-rounded" src="img/20242/12_small.jpg" />
                    </a>
                    <a href="img/20242/13.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/20242/13_small.jpg" />
                    </a>
                    <a href="img/20242/14.jpg" class="col-sm-3  col-xs-6">
                        <img class="img-rounded" src="img/20242/14_small.jpg" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
