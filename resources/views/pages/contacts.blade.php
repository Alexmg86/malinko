@extends('layouts.app')
@section('pageTitle', 'Владимир Малинко - Контакты')
@section('content')
<section class="easy">
    <div class="container">
        <h1>контакты</h1>
        <div class="pad-easy-30">
            <p>Ваши предложения Вы можете оставить на этой странице или отправить на электронный адрес: <a href="mailto:wladeymir@yandex.ru" class="link">wladeymir@yandex.ru</a>.<br>Контактный телефон: <strong>8-921-897-3453</strong> (Мегафон, Северо-западный филиал).</p>
        </div>
    </div>

    <div class="contacts-page">
        <div class="container">
            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if(session()->has('message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session()->get('message') }}
            </div>
            @endif
            {!! Form::open(['route' => 'mailsent', 'id' => 'feedbackForm']) !!}
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('name', 'Имя') !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('subj', 'Тема') !!}
                            {!! Form::text('subj', old('subj'), ['class' => 'form-control',]) !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('msg', 'Сообщение') !!}
                            {!! Form::textarea('msg', old('msg'), ['class' => 'form-control', 'rows' => '5']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Отправить сообщение</button>
                        </div>
                    </div>
                </div>
            {{Form::close()}}
        </div>
    </div>
</section>
@stop
