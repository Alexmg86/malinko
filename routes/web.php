<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index');
Route::get('/tvorchestvo', 'SiteController@tvorchestvo');
Route::get('/gallery', 'SiteController@gallery');
Route::get('/contacts', 'SiteController@contacts');
Route::post('/mailsent', 'SiteController@mailsent')->name('mailsent');
