<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailRequest;
use Illuminate\Http\Request;

class SiteController extends Controller
{
	/**
	 * Вывод главной страницы
	 */
    public function index()
    {
    	return view('pages.index');
    }

    /**
	 * Вывод страницы творчества
	 */
    public function tvorchestvo()
    {
    	return view('pages.tvorchestvo');
    }

    /**
	 * Вывод страницы фото
	 */
    public function gallery()
    {
    	return view('pages.gallery');
    }

    /**
     * Вывод страницы контакты
     */
    public function contacts()
    {
        return view('pages.contacts');
    }

    /**
	 * Вывод страницы контакты
	 */
    public function mailsent(MailRequest $request)
    {
        /* Configuration */
        $subject = 'Заявка с сайта'; // Set email subject line here
        $mailto  = 'wladeymir@yandex.ru'; // Email address to send form submission to
        /* END Configuration */

        $name = $request->name;
        $email = $request->email;
        $subj = $request->subj;
        $message = $request->msg;
        $timestamp = date("F jS Y, h:iA.", time());

        // HTML for email to send submission details
        $body = "
        <br>
        <p>Заявка с сайта</p>
        <p><strong>Имя</strong>: $name</p>
        <p><strong>Email</strong>: $email</p>
        <p><strong>Тема</strong>: $subj</p>
        <p><strong>Сообщение</strong>: $message</p>
        <hr/>
        <p>Форма отправлена <strong>$timestamp</strong></p>
        ";

        $headers = "From: $name <$email> \r\n";
        $headers .= "Reply-To: $email \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $message = "<html><body>$body</body></html>";

        if (mail($mailto, $subject, $message, $headers)) {
            $m = "Спасибо! Ваше сообщение успешно отправлено!";
        } else {
            $m = 'Ошибка... Попробуйте снова';
        }
        return redirect()->back()->with('message', $m);
    }
}
