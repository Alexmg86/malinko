<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'subj' => 'required',
            'msg' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле "Имя" обязательно для заполнения',
            'email.required' => 'Поле "E-mail" обязательно для заполнения',
            'email.email' => 'Поле "E-mail" имеет неверный формат',
            'subj.required' => 'Поле "Тема" обязательно для заполнения',
            'msg.required' => 'Поле "Сообщение" обязательно для заполнения'
        ];
    }
}
